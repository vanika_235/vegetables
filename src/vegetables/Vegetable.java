/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetables;

public abstract class Vegetable {
    private String colour;
    private double size;
    
    public Vegetable (String colour, double size){
        this.colour= colour;
        this.size=size;
        
    }
    public String getColour(){
        return colour;
    }
    
    public double getSize(){
        return size;
    }
    public abstract boolean isRipe();
    
}    

