/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetables;

public class VegetableSimulation {
    public static void main (String args[]){
        Beet beetBaby= new Beet ("Green",1);
        Carrot carrotBaby=new Carrot("Yellow",1);
        Beet beetRipe= new Beet ("Red",3);
        Carrot carrotRipe=new Carrot("Orange", 2);
    
        VegetableFactory factory= VegetableFactory.getInstance();
         Vegetable ripeCarrot= factory.getVegetable(VegetableType.CARROT , "orange",3);
        System.out.println("carrot is ripe?" + ripeCarrot.isRipe());
        
         Vegetable unripeCarrot= factory.getVegetable(VegetableType.CARROT , "yellow",1);
        System.out.println("carrot is ripe?" + ripeCarrot.isRipe());
        
         Vegetable ripeBeet= factory.getVegetable(VegetableType.BEET , "red",3);
        System.out.println("beet is ripe?" + ripeBeet.isRipe());
        
         Vegetable unripeBeet= factory.getVegetable(VegetableType.BEET , "green",1);
        System.out.println("beet is ripe?" + ripeBeet.isRipe());
    }   
}
