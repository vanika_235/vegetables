/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetables;


public class Beet extends Vegetable{
    public Beet (String colour, double size){
        super(colour,size);
    }
    public boolean isRipe(){
        return "red".equalsIgnoreCase(getColour())&& getSize()>=2;
    }
}
