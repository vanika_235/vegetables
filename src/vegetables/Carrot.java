/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetables;

public class Carrot extends Vegetable {
    
    public Carrot (String colour, double size){
        super(colour,size);
    }
    public boolean isRipe(){
        return "orange".equalsIgnoreCase(getColour())&& getSize()>=1.5;
    }
}
