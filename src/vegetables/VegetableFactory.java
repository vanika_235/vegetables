/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetables;

public class VegetableFactory {
      private static VegetableFactory vegFactory;
      private VegetableFactory(){
          
      }
   
      public static VegetableFactory getInstance(){
          if(vegFactory==null){
              vegFactory= new VegetableFactory();
          }
          return vegFactory;
          
      }
      
      public Vegetable getVegetable(VegetableType type, String colour, double size){
     switch (type){
       case CARROT:return new Carrot (colour,size);
       case    BEET:return new Beet (colour,size);
}
return null;
      }
}